# -*- coding: utf-8 -*-
##
##
#
# Start the nmap scan and wait for it to complete.
# Then format the resonse and send the data back to the server
#
import subprocess
import os
import xmltodict, json
import csv




def start_scan():
    '''
    Start the nmap scan of the target
    '''
    print("Starting scan")
    process = subprocess.Popen('/opt/arachni/bin/arachni --audit-forms {0} --report-save-path=/result.afr  --output-verbose'.format(os.environ['TARGET']) , shell=True, stdout=subprocess.PIPE)
    for stdout_line in iter(process.stdout.readline, ""):
        print(stdout_line)
        pass 
    process.stdout.close()
    return_code = process.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)    
    





def convert_output():
    '''
    Converts the output to json
    '''
    print("Convert result")
    process = subprocess.Popen('/opt/arachni/bin/./arachni_reporter /result.afr --reporter=json:outfile=/report.json' , shell=True, stdout=subprocess.PIPE)
    process.wait()


    with open('/report.json') as f:
        d = json.load(f)
        #print(d['issues'])

        for issue in d['issues']:
            issue['type']="Arachni Scanner"
            issue['target']= os.environ['TARGET']
            print(json.dumps(issue))
#Start the scan and get the output
start_scan()
convert_output()
